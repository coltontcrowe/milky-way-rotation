# Milky Way Rotation

A research paper I wrote in college that involved using a radio telescope to calculate the rotation speed of the Milky Way.  It was done as part of an astronomy lab course.

Raw data was collected with the radio telescope, analyzed in an ipython notebook (back before it became jupyter) and then analyzed for the final report.  You can see the final report [here](https://gitlab.com/coltontcrowe/milky-way-rotation/-/blob/master/rotationmilkyway.pdf)