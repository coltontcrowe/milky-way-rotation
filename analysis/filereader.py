'''made to read the spetroscopy files'''

from __future__ import division
from numpy import *

class FileReader(object):
    '''Reads a spectroscopy output and stores
    the relevant data in an object.'''
    def __init__(self, filename):
        self.filename = 'Data/' +  filename
        # first, put the raw data in a list
        with  open(self.filename, 'r') as f:
            self.rawdata = f.readlines()

        #now we organize this into lines that contain
        #individual parts deliniated by spaces
        self.orgdata=[]
        for line in self.rawdata:
            linedata = line.split()
            if not linedata[0] == 'No':
                self.orgdata.append(linedata)

        #remove lines that are not necessary
        for i in range(12):
            self.orgdata.pop(0)

        ind = self.orgdata[0].index('l(deg):')
        self.l = self.orgdata[0][ind+1]
        self.b = self.orgdata[0][ind+3]
        self.orgdata.pop(0)
        self.orgdata.pop(0)

        self.indices = []
        self.amplitudes = []
        self.frequencies = []
        for line in self.orgdata:
            self.indices.append(double(line[0]))
            self.amplitudes.append(double(line[1]))
            self.frequencies.append(double(line[2]))
        
        #save the radial velocity correction
        self.vcorrection = None
        correctionname = ('Data/corrections/' + filename).replace('.out', '_doppler_velocities.txt')
        with open(correctionname, 'r') as f:
            correctiondata = f.readlines()
            if len(correctiondata) > 1:
                self.vcorrection = double((correctiondata[1].split())[8])

class FileGroup(object):
    '''groups files of the same type and has operations
    for plotting and analysis'''
    def __init__(self,filereaders):
        for fr in filereaders:
            print 'using data from' + fr.filename
        
        #print lattitude and longitude
        for fr in filereaders:
            print 'l:' + fr.l
            print 'b:' + fr.b
        print 'check these for similarities and differences'
        
        self.amplitudearray=[]
        for fr in filereaders:
            self.amplitudearray.append(fr.amplitudes)
        self.amplitudearray = array(self.amplitudearray)
        
        ampsum = zeros(len(self.amplitudearray[0]))
        for i in range(len(self.amplitudearray)):
            ampsum += self.amplitudearray[i]
        
        self.meanamp = ampsum/len(filereaders)

        #get an array of standard deviations
        sumdifsqr = zeros(len(self.amplitudearray[0]))
        for i in range(len(self.amplitudearray)):
            sumdifsqr += (self.amplitudearray[i] - self.meanamp)**2
        self.stddevamp = (sumdifsqr/len(filereaders))**0.5

        self.frequencies = filereaders[0].frequencies

